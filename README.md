# Assigment 

Kotlin written, Gradle built. 
Do not use any 3rd party Library, except for the data persistency layer. Idea is to keep the project as vanilla as possible.

Connects to the Github API to retrieve the list of public repositories in your Github Account and visualizes the results in a list.
Alternatively, use this account: https://api.github.com/users/mralexgray/repos

Once the list has been populated, start retrieving information about the last commit for each repository.
This can be done with the following call: https://api.github.com/repos/mralexgray/ACEView/commits

Architecturally it is split into several independent layers, and also UI part implements MVP pattern which I found an easiest
in this case and given time frames. As required, zero libraries were used that appeared to be a bit hard to do.