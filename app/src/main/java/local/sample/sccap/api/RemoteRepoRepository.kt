package local.sample.sccap.api

/**
 * Fetching list of repositories from a specified page
 */
class RemoteRepoRepository : Repositories {

    override fun getRemoteRepositories(currentPage: Int): List<Repo> =
            executeGetListOfReposRequest(currentPage)
}