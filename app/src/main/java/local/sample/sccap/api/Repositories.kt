package local.sample.sccap.api

interface Repositories {
    fun getRemoteRepositories(currentPage: Int): List<Repo>
}

