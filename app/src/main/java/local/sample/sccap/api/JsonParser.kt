package local.sample.sccap.api

import android.util.JsonReader

/**
 * Parsing JSON from API response and converting into a specified model
 * @see Repo
 *
 */
fun readRepoArray(reader: JsonReader): List<Repo> {
    val messages = ArrayList<Repo>()

    reader.beginArray()
    while (reader.hasNext()) {
        messages.add(readRepo(reader))
    }
    reader.endArray()
    return messages
}

fun readRepo(reader: JsonReader): Repo {
    var repoId: Int? = null
    var fullName: String? = null
    var address: String? = null

    reader.beginObject()
    while (reader.hasNext()) {
        val name = reader.nextName()
        if (name == REPO_ID) {
            repoId = reader.nextInt()
        } else if (name == REPO_FULL_NAME) {
            fullName = reader.nextString()
        } else if (name == REPO_URL) {
            address = reader.nextString()
        } else {
            reader.skipValue()
        }
    }
    reader.endObject()

    return Repo(repoId, fullName, address)
}

data class Repo(
        val repoId: Int?,
        val fullName: String?,
        val address: String?
)

private const val REPO_ID = "id"
private const val REPO_FULL_NAME = "full_name"
private const val REPO_URL = "html_url"