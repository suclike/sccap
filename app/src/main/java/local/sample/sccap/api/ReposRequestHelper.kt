package local.sample.sccap.api

import android.util.JsonReader
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

private val urlRepos by lazy {
    "${BuildConfig.BASE_WS_URL}users/mralexgray/repos?per_page=10&${BuildConfig.GIT_HUB_AUTH_TOKEN}&${BuildConfig.GIT_HUB_AUTH_TOKEN_TYPE}"
}

/**
 * Opens a connection to a specified URL
 */
fun executeGetListOfReposRequest(currentPage: Int): List<Repo> {
    val url = URL("$urlRepos&page=$currentPage")
    val urlConnection = url.openConnection() as HttpURLConnection
    urlConnection.requestMethod = "GET"
    val statusCode = urlConnection.responseCode

    return if (statusCode == 200) {
        try {
            readRepoJsonStream(urlConnection.inputStream)
        } finally {
            urlConnection.disconnect()
        }
    } else {
        urlConnection.disconnect()
        emptyList()
    }
}

private fun readRepoJsonStream(stream: InputStream): List<Repo> {
    val reader = JsonReader(InputStreamReader(stream, "UTF-8"))
    reader.use { reader ->
        return readRepoArray(reader)
    }
}