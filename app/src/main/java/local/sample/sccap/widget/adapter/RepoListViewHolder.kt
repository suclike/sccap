package local.sample.sccap.widget.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.list_item_layout.view.*
import local.sample.sccap.R
import local.sample.sccap.widget.RepoUiModel

class RepoListViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val repoIdView: TextView = itemView.repoIdView
    private val repoFullNameView: TextView = itemView.repoFullNameView
    private val repoAddressView: TextView = itemView.repoAddressView
    private val repoRowCounterView: TextView = itemView.repoRowCounterView

    fun bind(repoUIModel: RepoUiModel, position: Int) {
        with(repoUIModel) {
            if (repoId != 0) {
                repoIdView.text = repoId.toString()
            }
            if (fullName.isNotEmpty()) {
                repoFullNameView.text = fullName
            }
            if (address.isNotEmpty()) {
                repoAddressView.text = address
            }

            repoRowCounterView.text = position.toString()
        }
    }

    companion object {

        fun create(viewGroup: ViewGroup): RepoListViewHolder {
            return RepoListViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.list_item_layout,
                    viewGroup, false))
        }
    }
}
