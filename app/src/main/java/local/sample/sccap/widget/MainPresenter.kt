package local.sample.sccap.widget

import android.os.Handler
import android.os.Looper
import android.util.Log
import local.sample.sccap.BuildConfig
import local.sample.sccap.domain.GetRepositoriesAction
import java.lang.Thread.NORM_PRIORITY
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.ThreadFactory

class MainPresenter(private val view: MainView) {

    private val handler by lazy { Handler(Looper.getMainLooper()) }
    private val executorService: ExecutorService by lazy {
        Executors.newFixedThreadPool(DEFAULT_THREAD_POOL_SIZE, SimpleThreadFactory())
    }

    private val runningTaskList: MutableList<Future<*>> = mutableListOf()

    private var listLoadingRunningTaskFuture: Future<*>? = null

    private var currentLoadedPage: Int = 0

    private val getRepositoriesAction by lazy {
        GetRepositoriesAction()
    }

    fun scrollChanged(page: Int) {
        loadListOfRepositories(page)
    }

    fun loadListOfRepositories(currentPage: Int) {
        val isLoadingFirstPage = isLoadingFirstPage(currentPage)
        if (isLoadingFirstPage) {
            view.showProgress()
        }

        if (!isLoadingFirstPage && currentLoadedPage == currentPage) {
            return
        }

        val loadListRunnable = Runnable {
            val listOfRepos: List<RepoUiModel> =
                    getRepositoriesAction.getListOfRepositories(currentPage)

            val runnableToUpdateView =
                    when {
                        isLoadingFirstPage && listOfRepos.isEmpty() -> Runnable {
                            view.hideProgress()
                            view.resetPagination()

                            view.showEmptyListMessage()
                        }
                        listOfRepos.isEmpty() -> Runnable {
                            view.notifyEmptyListLoaded()
                            view.setPageLoadFailed()
                        }
                        isLoadingFirstPage && listOfRepos.isNotEmpty() ->
                            Runnable {
                                updateCurrentLoadedPage(currentPage)

                                view.hideProgress()
                                view.hideEmptyErrorMessage()
                                view.showMainList()
                                view.setupCounterInToolbar()
                                view.showListOfRepositories(listOfRepos)
                            }
                        else -> Runnable {
                            updateCurrentLoadedPage(currentPage)

                            view.setupCounterInToolbar()
                            view.appendListOfRepositories(listOfRepos)
                        }
                    }

            handler.post {
                runnableToUpdateView.run()
            }
        }

        listLoadingRunningTaskFuture = executorService.submit {
            loadListRunnable.run()
        }

        listLoadingRunningTaskFuture?.let { currentFuture ->
            runningTaskList.add(currentFuture)
        }
    }

    fun stopCurrentLoadingTasks() {
        listLoadingRunningTaskFuture?.cancel(true)
        runningTaskList.clear()
    }

    fun cleanUpWhenDestroyed() {
        listLoadingRunningTaskFuture = null
        runningTaskList.clear()
        executorService.shutdownNow()
    }

    private fun updateCurrentLoadedPage(currentPage: Int) {
        currentLoadedPage = currentPage
    }

    private fun isLoadingFirstPage(currentPage: Int): Boolean = currentPage == 0
}

class SimpleThreadFactory : ThreadFactory {
    override fun newThread(runnable: Runnable?): Thread {
        val thread = Thread(runnable)
        return thread.apply {
            isDaemon = true
            priority = NORM_PRIORITY

            uncaughtExceptionHandler =
                    Thread.UncaughtExceptionHandler { thread, ex ->
                        if (BuildConfig.DEBUG) {
                            Log.e("Thread execution error", thread.name + " encountered an error: " + ex.message)
                        }
                    }
        }
    }
}

private const val DEFAULT_THREAD_POOL_SIZE = 1
