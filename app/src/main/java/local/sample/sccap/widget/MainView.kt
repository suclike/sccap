package local.sample.sccap.widget


interface MainView {

    fun showListOfRepositories(listOfRepos: List<RepoUiModel>)

    fun appendListOfRepositories(listOfRepos: List<RepoUiModel>)

    fun setupCounterInToolbar()

    fun showEmptyListMessage()

    fun notifyEmptyListLoaded()

    fun showMainList()

    fun hideEmptyErrorMessage()

    fun showProgress()
    fun hideProgress()

    fun setPageLoadFailed()
    fun resetPagination()
}