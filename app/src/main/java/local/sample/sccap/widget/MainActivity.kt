package local.sample.sccap.widget

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import local.sample.sccap.R
import local.sample.sccap.util.hide
import local.sample.sccap.util.show
import local.sample.sccap.widget.adapter.RepoListRecycleViewAdapter

class MainActivity : AppCompatActivity(), MainView {

    private val presenter: MainPresenter by lazy {
        MainPresenter(this)
    }

    private val recyclerViewLayoutManager by lazy {
        LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    private val recycleViewAdapter by lazy {
        RepoListRecycleViewAdapter.create()
    }

    private val paginationScrollListener: PaginationScrollListener by lazy {
        PaginationScrollListener { page, _, _ ->
            presenter.scrollChanged(page)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecycleView()
    }

    override fun onStart() {
        super.onStart()

        val lastLoadedPage = paginationScrollListener.currentPage
        presenter.loadListOfRepositories(lastLoadedPage)
    }

    override fun onStop() {
        super.onStop()

        presenter.stopCurrentLoadingTasks()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.cleanUpWhenDestroyed()
    }

    override fun showListOfRepositories(listOfRepos: List<RepoUiModel>) {
        recycleViewAdapter.setListOfRepos(listOfRepos)
    }

    override fun appendListOfRepositories(listOfRepos: List<RepoUiModel>) {
        recycleViewAdapter.appendListOfRepos(listOfRepos)
    }

    override fun setupCounterInToolbar() {
        mainToolbar.title = String.format(getString(R.string.toolbar_counter), recycleViewAdapter.itemCount)
    }

    override fun showMainList() {
        reposRecyclerView.show()
    }

    override fun showProgress() {
        progressBar.show()
    }

    override fun hideProgress() {
        progressBar.hide()
    }

    override fun hideEmptyErrorMessage() {
        emptyListMessage.hide()
    }

    override fun showEmptyListMessage() {
        emptyListMessage.show()
    }

    override fun notifyEmptyListLoaded() {
        Toast.makeText(this, "No new repos loaded", Toast.LENGTH_SHORT).show()
    }

    override fun resetPagination() {
        paginationScrollListener.resetState()
    }

    override fun setPageLoadFailed() {
        paginationScrollListener.lastLoadFailed = true
    }

    private fun initRecycleView() {
        with(reposRecyclerView) {
            setHasFixedSize(true)
            setItemViewCacheSize(5)
            layoutManager = recyclerViewLayoutManager
            paginationScrollListener.layoutManager = recyclerViewLayoutManager
            addItemDecoration(DividerItemDecoration(applicationContext,
                    recyclerViewLayoutManager.orientation))
            adapter = recycleViewAdapter

            addOnScrollListener(paginationScrollListener)
        }
    }
}