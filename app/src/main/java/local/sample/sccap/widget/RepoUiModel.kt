package local.sample.sccap.widget

data class RepoUiModel(
        val repoId: Int,
        val fullName: String,
        val address: String
)