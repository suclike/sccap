package local.sample.sccap.widget.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import local.sample.sccap.widget.RepoUiModel

class RepoListRecycleViewAdapter private constructor()
    : RecyclerView.Adapter<RepoListViewHolder>() {

    private val adapterRepoList by lazy { mutableListOf<RepoUiModel>() }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoListViewHolder {
        return RepoListViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RepoListViewHolder, position: Int) {
        holder.bind(adapterRepoList[position], position)
    }

    override fun getItemCount(): Int {
        return adapterRepoList.size
    }

    fun setListOfRepos(listOfRepos: List<RepoUiModel>) {
        adapterRepoList.addAll(listOfRepos)
        notifyDataSetChanged()
    }

    fun appendListOfRepos(listOfRepos: List<RepoUiModel>) {
        val oldRepoCount = adapterRepoList.size
        adapterRepoList.addAll(listOfRepos)
        val newRepoCount = adapterRepoList.size
        notifyItemRangeInserted(oldRepoCount, newRepoCount)
    }

    companion object {

        fun create(): RepoListRecycleViewAdapter {
            return RepoListRecycleViewAdapter()
        }
    }
}