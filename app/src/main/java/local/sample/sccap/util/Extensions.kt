package local.sample.sccap.util

import android.view.View

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun Int?.orZero(): Int = this ?: DEFAULT_INT

private const val DEFAULT_INT = 0
