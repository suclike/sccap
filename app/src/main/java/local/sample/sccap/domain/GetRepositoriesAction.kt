package local.sample.sccap.domain

import local.sample.sccap.api.RemoteRepoRepository
import local.sample.sccap.api.Repositories
import local.sample.sccap.util.orZero
import local.sample.sccap.widget.RepoUiModel

/**
 * Request a list of repositories from remote repository and apply transformation of a domain to a
 * UI model with default parameters
 */
class GetRepositoriesAction(
        private val remoteRepoRepository: Repositories = RemoteRepoRepository()) : RepositoriesAction {

    override fun getListOfRepositories(currentPage: Int): List<RepoUiModel> =
            remoteRepoRepository.getRemoteRepositories(currentPage)
                    .map { repo ->
                        with(repo) {
                            RepoUiModel(
                                    repoId = repoId.orZero(),
                                    fullName = fullName.orEmpty(),
                                    address = address.orEmpty()
                            )
                        }
                    }
}