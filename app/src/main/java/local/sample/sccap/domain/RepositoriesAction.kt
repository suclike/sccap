package local.sample.sccap.domain

import local.sample.sccap.widget.RepoUiModel


interface RepositoriesAction {

    fun getListOfRepositories(currentPage: Int = 0): List<RepoUiModel>
}